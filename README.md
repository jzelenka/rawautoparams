## RawAutoParams

RawAutoParams is an extension for RawPrassLib library which provides detailed readout of acquisiton parameters.

## Tested instruments
This code has been briefly tested on TSQ-7000, LCQ Deca and LCQ Advantage Max

## Known drawbacks
This code is experimental. Do not expect it will work in your case out of the box or you'll be disappointed.
